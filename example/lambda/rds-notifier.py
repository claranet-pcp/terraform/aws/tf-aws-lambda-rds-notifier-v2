import json
import os
import requests
from urllib.request import HTTPError
import boto3

# Current webhook points at: #uk-cbu-team-mario-notify
# Waiting on the webhook to be approved and then this will be updated to: #uk-techsol-aws-notifications
SLACK_WEBHOOK_URL = "https://hooks.slack.com/services/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"

def lambda_handler(event, context):
    rds_message = json.loads(event["Records"][0]["Sns"]["Message"])
    source_type = rds_message["Event Source"]
    resource_id = rds_message["Source ID"]

    if "db-instance" in source_type:
        source_type = "db-instance"
    elif "db-cluster" in source_type:
        source_type = "db-cluster"
    else:
        source_type = "unknown"

    engine_version = get_db_engine_versions(source_type, resource_id)
    send_slack_notification(SLACK_WEBHOOK_URL, rds_message, source_type, resource_id, engine_version)

def get_db_instance_engine_version(instance_id):
    rds_client = boto3.client("rds")
    response = rds_client.describe_db_instances(DBInstanceIdentifier=instance_id)
    return response["DBInstances"][0]["EngineVersion"]

def get_db_cluster_engine_version(cluster_id):
    rds_client = boto3.client("rds")
    response = rds_client.describe_db_clusters(DBClusterIdentifier=cluster_id)
    return response["DBClusters"][0]["EngineVersion"]

def get_db_engine_versions(source_type, resource_id):
    if source_type == "db-instance":
        return get_db_instance_engine_version(resource_id)
    elif source_type == "db-cluster":
        return get_db_cluster_engine_version(resource_id)
    else:
        return "unknown"

def get_db_instance_engine_name(instance_id):
    rds_client = boto3.client("rds")
    response = rds_client.describe_db_instances(DBInstanceIdentifier=instance_id)
    return response["DBInstances"][0]["Engine"]

def get_cluster_engine_name(cluster_id):
    rds_client = boto3.client("rds")
    response = rds_client.describe_db_clusters(DBClusterIdentifier=cluster_id)
    return response["DBClusters"][0]["Engine"]

def get_engine_name(source_type, resource_id):
    if source_type == "db-instance":
      return get_db_instance_engine_name(resource_id)
    elif source_type == "db-cluster":
      return get_cluster_engine_name(resource_id)
    else:
      return "unknown"

def send_slack_notification(webhook_url, rds_message, source_type, resource_id, engine_version):
    event_details = rds_message["Event Message"]
    env_type = os.environ["ENVTYPE"]
    env_name = os.environ["ENVNAME"]
    customer = os.environ["CUSTOMER"]

    payload = {
        "blocks": [
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": f"<!channel> *RDS Event:* {event_details}",
                },
            },
            {
                "type": "section",
                "fields": [
                    {"type": "mrkdwn", "text": f"*RDS {source_type.upper()} ID:*\n{resource_id}"},
                    {"type": "mrkdwn", "text": f"*Customer:*\n{customer}"},
                    {"type": "mrkdwn", "text": f"*Engine Version:*\n{engine_version}"},
                    {"type": "mrkdwn", "text": f"*Environment Name:*\n{env_name}"},
                    {"type": "mrkdwn", "text": f"*DB Type:*\n{get_engine_name(source_type, resource_id)}"},
                    {"type": "mrkdwn", "text": f"*Environment Type:*\n{env_type}"},
                ],
            },
            {
            "type": "divider",
            },
        ]
    }

    try:
        response = requests.post(
            webhook_url, json=payload, headers={"Content-Type": "application/json"}
        )
        response.raise_for_status()
    except HTTPError as http_err:
        print(f"HTTP error occurred: {http_err}")
    except Exception as err:
        print(f"An error occurred: {err}")

if __name__ == "__main__":
    test_event = {
        "Records": [
            {
                "Sns": {
                    "Message": '{"Event Message": "Engine Version Changed", "Event Source": "db-instance", "Source ID": "test1"}'
                }
            }
        ]
    }
    lambda_handler(test_event, {})