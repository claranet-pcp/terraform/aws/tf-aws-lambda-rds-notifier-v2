provider "aws" {
  region = "eu-west-1"
}

module "rds_notifier" {
  source = "../"

  lambda_source_path = "${path.module}/lambda/"
  envtype            = "test"
  envname            = "gb-test"
  customer_name      = "technical solutions cloud"
}