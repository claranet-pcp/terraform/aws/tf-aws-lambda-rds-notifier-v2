variable "envtype" {
  description = "Environment type"
  type        = string
  default     = "test"
}

variable "envname" {
  description = "Environment name"
  type        = string
  default     = "test"
}

variable "customer_name" {
  description = "Customer name"
  type        = string
  default     = "technical solutions cloud"
}

variable "lambda_source_path" {
  description = "Path to the lambda source code"
  type        = string
}

variable "slack_webhook_url" {
  description = "The Slack webhook URL for sending RDS notifications"
  type        = string
  # default     = "https://hooks.slack.com/services/AAA/BBB/CCC"
}

variable "recreate_missing_package" {
  description = "Whether to recreate missing Lambda package if it is missing locally or not"
  type        = bool
  default     = true
}