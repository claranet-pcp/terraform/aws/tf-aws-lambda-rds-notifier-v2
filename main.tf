resource "aws_db_event_subscription" "engine_version_upgrade" {
  name        = "engine-version-upgrade"
  sns_topic   = aws_sns_topic.rds_notices.arn
  source_type = "db-instance"

  event_categories = [
    "maintenance"
  ]
}

resource "aws_db_event_subscription" "cluster_engine_version_upgrade" {
  name        = "cluster-engine-version-upgrade"
  sns_topic   = aws_sns_topic.rds_notices.arn
  source_type = "db-cluster"

  event_categories = [
    "maintenance"
  ]
}

resource "aws_sns_topic" "rds_notices" {
  name = "rds-notices"
}

resource "aws_sns_topic_subscription" "lambda" {
  topic_arn = aws_sns_topic.rds_notices.arn
  protocol  = "lambda"
  endpoint  = module.rds_notifier.lambda_function_arn
}

module "rds_notifier" {
  source  = "terraform-aws-modules/lambda/aws"
  version = "7.4.0"

  function_name                  = "rds-notifier"
  description                    = "rds-notifier"
  handler                        = "rds-notifier.lambda_handler"
  runtime                        = "python3.11"
  timeout                        = 300
  source_path                    = var.lambda_source_path
  recreate_missing_package       = var.recreate_missing_package

  attach_policy_statements = true
  policy_statements = {

    rds_describe_db_instances = {
      actions   = ["rds:DescribeDBInstances", "rds:DescribeDBClusters"]
      effect    = "Allow"
      resources = ["*"]
    }
  }

  environment_variables = {
    ENVTYPE           = var.envtype
    ENVNAME           = var.envname
    CUSTOMER          = var.customer_name
    SLACK_WEBHOOK_URL = var.slack_webhook_url
  }
}

resource "aws_lambda_permission" "allow_sns" {
  statement_id  = "AllowExecutionFromSNS"
  function_name = module.rds_notifier.lambda_function_arn
  action        = "lambda:InvokeFunction"
  principal     = "sns.amazonaws.com"
  source_arn    = aws_sns_topic.rds_notices.arn
}