output "sns_topic_arn" {
  value       = aws_sns_topic.rds_notices.arn
  description = "The ARN of the SNS topic created"
}

output "lambda_function_arn" {
  value       = module.rds_notifier.lambda_function_arn
  description = "The ARN of the Lambda function"
}