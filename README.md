# RDS Notifier Module

This Terraform module deploys a serverless RDS notifier that sends Slack notifications when an Amazon RDS instance or cluster gets updated.

## Features

- Configurable event subscription for RDS instances and clusters
- Lambda function that processes incoming RDS event notifications
- Sends Slack notifications with relevant information about RDS events

## Usage

```hcl
module "rds_notifier" {
  source = "git::git@gitlab.com:claranet-pcp/terraform/aws/tf-aws-lambda-rds-notifier.git"

  lambda_source_path = "${path.module}/.terraform/modules/rds_notifier/lambda"
  envtype            = var.envtype
  envname            = var.envname
  customer_name      = var.customer_name
  slack_webhook_url  = var.slack_webhook_url
}
```

## Requirements

| Name      | Version   |
|-----------|-----------|
| terraform | >= 1.x.x  |
| aws       | >= 5.x.x  |

## Providers

| Name | Version   |
|------|-----------|
| aws  | >= 5.32.0 |

## Inputs

| Name               | Description                    | Type   | Default                       | Required |
|--------------------|--------------------------------|--------|-------------------------------|:--------:|
| envtype            | Environment type               | string | `"test"`                      | no       |
| envname            | Environment name               | string | `"test"`                      | no       |
| customer_name      | Customer name                  | string | `"technical solutions cloud"` | no       |
| lambda_source_path | Path to the lambda source code | string | n/a                           | yes      |

## Outputs

| Name                | Description                                                                     |
|---------------------|---------------------------------------------------------------------------------|
| sns_topic_arn       | The Amazon Resource Name (ARN) of the SNS topic for RDS event notifications     |
| lambda_function_arn | The Amazon Resource Name (ARN) of the Lambda function that processes RDS events |

## Lambda Function

The Lambda function is equipped with the following environment variables:

- ENVTYPE: The environment type
- ENVNAME: The environment name
- CUSTOMER: The customer's name

The Lambda function should be written in Python 3.9.